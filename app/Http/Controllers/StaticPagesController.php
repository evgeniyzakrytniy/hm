<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticPagesController extends Controller
{
    public function aboutPage()
    {
    	$name = "DATA.ua";
		$slogan = "Национальный портал новостей";

		$people = [
			'Jeniya', 'Iryna', 'Tatyana'
		];

		$params = app( '\Aimeos\Shop\Base\Page' )->getSections( 'mypage' );
		dump($params);

    	return view('layouts.static.about', 

    		compact(
	    		'name', 
	    		'slogan', 
	    		'people'
	    	)

	    );
    }
}
